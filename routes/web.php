<?php

use App\Http\Controllers\UtsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('data',[UtsController::class, 'index'])->name('data');
route::get('daftar' , [UtsController::class, 'create'])->name('daftar');
route::post('daftar' , [UtsController::class, 'store'])->name('daftar');
route::get('edit/{id}' , [UtsController::class, 'edit'])->name('edit');
route::put('edit/{id}' , [UtsController::class, 'update'])->name('edit');
route::delete('delete/{id}' , [UtsController::class, 'destroy'])->name('delete');

