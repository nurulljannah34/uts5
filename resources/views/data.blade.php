<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<div class="post-content">
    <a href="{{route('daftar')}}"><button>Tambah Data</button></a>
    <h2> input data siswa</h2>
    <table>
        <thead>
            <tr>
            <th>Npm</th>
            <th>Nama</th>
            <th>Jenis_kelamin</th>
            <th>TTL</th>
            <th>Agama</th>
            <th>Alamat</th>
            <th>Hobby</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)    
            <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->Nim}}</td>
            <td>{{$item->Nama}}</td>
            <td>{{$item->Jenis_kelamin}}</td>
            <td>{{$item->TTL}}</td>
            <td>{{$item->Agama}}</td>
            <td>{{$item->Alamat}}</td>
            <td>{{$item->Hoby}}</td>      
            <td>
                <a href="{{route('edit' , $item->id)}}">
                    <button id="form-user">edit</button>
                </a>
                <form method="POST" action="{{ route('delete', $item->id) }}" id="hapus">
                    @csrf
                    @method('DELETE')
                    <div class="d-grid gap-2">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('yakin?')">Hapus</button>
                    </div>
                </form> 
            
            </td>

            </tr>
            @endforeach
        </tbody>
		</body>
		</html>