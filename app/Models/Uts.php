<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uts extends Model
{
    use HasFactory;
    protected $fillable=[
        'Nim',
        'Nama',
        'Jenis_kelamin',
        'TTL',
        'Agama',
        'Alamat',
        'Hobby',
    ];
}
