<?php

namespace App\Http\Controllers;

use App\Models\Uts;
use Illuminate\Http\Request;

class UtsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Uts::all();
        return view('data' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Uts;
        $save = $data->create([
            'Nim' => $request->nim,
            'Nama' => $request->nama,
            'Jenis_kelamin' => $request->jenis_kelamin,
            'TTL' => $request->ttl,
            'Agama' => $request->agama,
            'Alamat' => $request->alamat,
            'Hobby' => $request->hobby,
        ]);
        if ($save){
            $data = Uts::all();
            return view('data' ,compact('data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Uts  $uts
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Uts  $uts
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Uts::find($id);
        return view('edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Uts  $uts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $data = Uts::find($id);
        $save = $data->update([
            'Nim' => $request->nim,
            'Nama' => $request->nama,
            'Jenis_kelamin' => $request->jenis_kelamin,
            'TTL' => $request->ttl,
            'Agama' => $request->agama,
            'Alamat' => $request->alamat,
            'Hobby' => $request->hobby,
        ]);
        if($save){
            $data = Uts::all();
            return view('data' ,compact('data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Uts  $uts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Uts::find($id);
        $delete = $data->delete();
        if ($delete){
            $data =Uts::all();
            return view('data', compact('data'));
        }
    }
}
